package be.janwagemakers.mgame;
/*
 * Een kaart is eigenlijk een JButton --> extends JButton
 */

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Kaart extends JButton {
	
	private int kaartnummer;	// nummer van de kaart. Nummer komt overeen met een bepaalde afbeelding
	private boolean zichtbaar;	// true als voorkant kaart zichtbaar is.
	private String style;

	public Kaart(int kaartnummer, String style) {
		this.kaartnummer=kaartnummer;
		this.style = style;
		toonAchterkant();
	}
	
	/*
	 * Wat is de kaartnummer?
	 */
	public int getKaartNummer() {
		return kaartnummer;
	}
	
	/*
	 * Is voorkant van de kaart zichtbaar?
	 */
	public boolean isZichtbaar() {
		return zichtbaar;
	}

	/*
	 * Toon de voorkant van de kaart.
	 */
	public void toonVoorkant() {
		zichtbaar = true;
		String afbeelding="/images/" + style + kaartnummer + ".png";
		setImage(afbeelding);
	}
	
	/*
	 * Toon de achterkant van de kaart.
	 */
	public void toonAchterkant() {
		zichtbaar = false;
		String afbeelding="/images/" + style + "0.png";
		setImage(afbeelding);
	}
	
	/*
	 * Verander de stijl van deze kaart.
	 */
	public void changeStyle(String style) {
		this.style = style;
		if (zichtbaar) { 
			toonVoorkant();
		} else {
			toonAchterkant();
		}
	}
    
	/*
	 * Schalen van de afbeelding naar een vaste afmeting
	 * 
	 * Zie : http://stackoverflow.com/questions/6714045/how-to-resize-jlabel-imageicon
	 * 
	 */
	private void setImage(String afbeelding) {
		ImageIcon imageIcon = new ImageIcon(Game.class.getResource(afbeelding)); 	// load the image to a imageIcon
		Image image = imageIcon.getImage(); 										// transform it 
		image = image.getScaledInstance(120, 120,  java.awt.Image.SCALE_SMOOTH); 	// scale it the smooth way  
		imageIcon = new ImageIcon(image);  											// transform it back
		setIcon(imageIcon);
	}
}
