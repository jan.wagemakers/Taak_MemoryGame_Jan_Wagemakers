/*
 * Actie uitvoeren wanneer er op een kaart wordt geklikt.
 * 
 * Bedoeling is dat er maar 1 instantie van deze klasse wordt gemaakt, 
 * zodat er ongeacht van op welke kaart er geklikt wordt we in dezelfde 
 * instantie terecht komen.
 *  
 */
package be.janwagemakers.mgame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class ActionClickOnCard implements ActionListener {
	
	private Game game;
	private boolean wait;				
	private boolean geenEersteKaart;
	private Kaart eersteKaart;
	private int counter;
				
	public ActionClickOnCard(Game game) {
		this.game = game;
		counter = Game.HOEVEEL;																	// counter = aantal kaarten op speelbord
		wait = false;
		geenEersteKaart = true;
	}
	
	/*
	 * Methode om geluid af te spelen.
	 */
	public void playSound(String wav) {
		// Gebaseerd op de info gevonden op: http://alvinalexander.com/java/java-audio-example-java-au-play-sound
		// open the sound file as a Java input stream
	    try {
			InputStream inputStream = getClass().getResourceAsStream("/sounds/" + wav);
			AudioStream audioStream = new AudioStream(inputStream);
		    AudioPlayer.player.start(audioStream);
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    // System.out.println(wav);
	}

	@Override
	public void actionPerformed(ActionEvent click) {
		game.scoreMin();																		// bij elke click wordt score met 1 verminderd
								
		if (wait) return;																		// Helaba, niet klikken als er gewacht moet worden.
																								// en als straf ben je nu een punt kwijt ook.
		
		/*
		 * Info gevonden op http://stackoverflow.com/questions/17624008/jbutton-array-actionlistener
		 * 
		 * Op deze manier kunnen we eenvoudig achterhalen op welke kaart er juist geklikt is
		 */
		Kaart kaart = ((Kaart) click.getSource()); 	
		if (kaart.isZichtbaar()) return;														// Klikken op een reeds zichtbare kaart doet niets.
		
		kaart.toonVoorkant();																	// Toon de voorzijde van de kaart
		
		if (geenEersteKaart) {																	// Was er al op een kaart geklikt?			
			eersteKaart = kaart;																// Nee, dan is dit de eerste kaart
			geenEersteKaart = false;
			playSound("click.wav");
			return;
		} 
		
		int eersteKaartNR = eersteKaart.getKaartNummer();										// Is de eerste kaart waar op geklikt is dezelfde als de tweede kaart?
		if (eersteKaartNR == kaart.getKaartNummer()) {											// Als gelijk, dan....
			counter = counter - 2;																				// - zijn er twee kaarten minder om te zoeken
			game.scorePlus();
			game.scorePlus();                                                                                   // - 2 punten bij omdat er twee kaarten zijn gevonden
			eersteKaart.setBackground(Color.GREEN);																// - passen we de kleur van de gevonden kaarten aan
			kaart.setBackground(Color.GREEN);
			geenEersteKaart = true;																				// - is er geen eersteKaart meer
			// System.out.println(counter);
			if(counter == 0) {		
				playSound("fanfare.wav");
				game.end();																			     		// - als we alle kaarten hebben gevonden is spel ten einde.
			} else {
				playSound("gong.wav");
			}
		} else {																				// Als ze niet gelijk zijn, dan....
			playSound("fail.wav");
			wait = true;
			Timer timer = new Timer();																			// - moet er gewacht worden om de tweede kaart een tijdje zichtbaar te houden
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					eersteKaart.toonAchterkant();																// - na de wachttijd tonen we de achterkant van de twee kaarten
					kaart.toonAchterkant();	
					geenEersteKaart = true;																		// - en is er dus ook geen eerste kaart meer
					wait = false;
				}
			}, 1*1000);																							// x * 1000ms
		}
	}
}
