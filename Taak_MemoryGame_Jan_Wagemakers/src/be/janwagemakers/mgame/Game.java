package be.janwagemakers.mgame;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Game {
	public static final int HOEVEEL = 16;								// Met hoeveel kaarten spelen we het spel
	public static final int START_SCORE = 100;							// De start score.
	private GUI gui;													
	private Scores scores;												// Om de hoogste scores bij te houden
	private int score;
	private ActionClickOnThema themaAction;
		
	public static void main(String[] args) {
		// Wanneer ik dit programma onder GNU/Linux test, had ik lelijke fonts.
		// Daarom System.setProperty toegevoegd. 
		//
		// Zie : http://stackoverflow.com/questions/179955/how-do-you-enable-anti-aliasing-in-arbitrary-java-apps
		// voor meer info.
		//
		// enable anti-aliased text:
		// System.setProperty("awt.useSystemAAFontSettings","on");
		// System.setProperty("swing.aatext", "true");
		//
		// Later, door in /etc/environment de lijn
		// 
		//			_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd'
		//
		// toe te voegen, is het probleem ook opgelost zonder dat System.setProperty nodig is.
		// Zie : https://wiki.archlinux.org/index.php/Java_Runtime_Environment_fonts
		new Game();
	}
	
	public Game() {
		gui = new GUI();												// Maak de GUI aan
		scores = new Scores();
		themaAction = new ActionClickOnThema(gui);
		gui.getChangeThemaJButton().addActionListener(themaAction);
		start();														// Start het spel		
		gui.setVisible(true);		
	}
	
	/*
	 * Hier komen we als alle kaarten zijn gevonden.
	 */
	public void end() {
		JOptionPane.showMessageDialog(null, "Joepie, je hebt ze allemaal gevonden!");
		scores.writeScores(score, gui.getNaamJTextField().getText());	// Schrijf de scores naar het bestand.
		gui.getGameBoard().removeAll();									// Verwijder alle kaarten van het speelbord
		start();
	}
	
	/*
	 * Starten van het spel. 
	 * 
	 * In deze methode worden de kaarten op het speelbord neergezet.
	 * 
	 */
	private void start() {
		// Maak een ArrayList met random paren
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i=1; i<=HOEVEEL/2; i++) {
			list.add(i);
			list.add(i);
		}
		Collections.shuffle(list);
		
		score = START_SCORE;
		updateScoreJLabel();											// Zet START_SCORE op het scherm
		scores.readScores();
		
		displayHighScores();											// Zet de hoogste scores op het scherm
					
		String style = themaAction.getThema();
								
		// Plaats de kaarten op het speelbord.
		JPanel speelBord = gui.getGameBoard();							// speelBord is waar de kaarten getoond worden
		ActionClickOnCard action = new ActionClickOnCard(this);			// Eén actionlistener voor alle kaarten
		for (int i : list) {
			Kaart kaart = new Kaart(i, style);
			kaart.addActionListener(action);							// Actie ondernemen als er op een kaart wordt geklikt. 
			speelBord.add(kaart);
		}
	}
	
	/*
	 * Tonen van de hoogste scores op het scherm.
	 */
	public void displayHighScores() {
		String lijst[]= scores.getScores();
		int counter = 0;
		for (String i : lijst) {
			if (i == null) break;
			String number = i.substring(0,  3);
			// System.out.println(number);
			String text = i.substring(4);
			// System.out.println(text);

			gui.getNamesJLabelArray()[counter].setText(text);
			gui.getScoresJLabelArray()[counter].setText(number);
			counter++;
		}
	}
	
	/*
	 * Score met 1 verhogen.
	 */
	public void scorePlus() {
		score++;			
		updateScoreJLabel();
	}
	
	/*
	 * Score met 1 verminderen
	 */
	public void scoreMin() {
		score--;														
		if (score < 0) score = 0;		// we werken niet met negatieve scores.
		updateScoreJLabel();
	}

	/*
	 * Score op het scherm updaten
	 */
	private void updateScoreJLabel() {
		gui.getScoreJLabel().setText("Score = " + score);
	}
}
