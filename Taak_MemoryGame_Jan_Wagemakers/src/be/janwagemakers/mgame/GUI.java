/*
 * Opbouwen van de GUI.
 */
package be.janwagemakers.mgame;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class GUI extends JFrame {

	private JTextField playerName;
	private JPanel gameBoard;
	private JLabel score;
	private JButton changeThema;
	private JLabel gekozenThema;
	private JLabel[] scoreLabel;
	private JLabel[] nameLabel;
		
	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 880, 800);						
		getContentPane().setLayout(null);
		
		gameBoard = new JPanel();
		gameBoard.setBounds(10, 95, 610, 610);				
		getContentPane().add(gameBoard);
		gameBoard.setLayout(new GridLayout(4, 4, 10, 10));		
		
		JPanel titelBalk = new JPanel();
		titelBalk.setBounds(10, 11, 844, 73);
		getContentPane().add(titelBalk);
		titelBalk.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel geheugenSpel = new JLabel("Geheugenspel");
		geheugenSpel.setFont(new Font("Dialog", geheugenSpel.getFont().getStyle() | Font.BOLD, 34));
		geheugenSpel.setHorizontalAlignment(SwingConstants.CENTER);
		titelBalk.add(geheugenSpel);
				
		JLabel hoogsteScores = new JLabel("Hoogste Scores");
		hoogsteScores.setFont(new Font("Dialog", hoogsteScores.getFont().getStyle() | Font.BOLD, 20));
		hoogsteScores.setBounds(630, 95, 224, 37);
		hoogsteScores.setForeground(Color.BLUE);
		hoogsteScores.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(hoogsteScores);
		
		/*
		 * In de zijbalk komen de scores + naam van de persoon
		 * 
		 * Bestaat uit twee JPanels met grid layout die naast elkaar staan,
		 * om zo alles mooi in lijn te zetten
		 * 
		 */
		JPanel zijBalkScores = new JPanel();
		zijBalkScores.setBounds(630, 143, 50, 250);
		getContentPane().add(zijBalkScores);
		zijBalkScores.setLayout(new GridLayout(0, 1, 0, 0));
		//
		JPanel zijBalkNaam = new JPanel();
		zijBalkNaam.setBounds(692, 144, 162, 250);
		getContentPane().add(zijBalkNaam);
		zijBalkNaam.setLayout(new GridLayout(0, 1, 0, 0));
		
		
		scoreLabel = new JLabel[10];
		nameLabel = new JLabel[10];
		boolean toggle = false;
		for (int counter = 0 ; counter < 10 ; counter++) {
			scoreLabel[counter] = new JLabel("---");
			scoreLabel[counter].setFont(new Font("Dialog", hoogsteScores.getFont().getStyle() | Font.BOLD, 15));
			scoreLabel[counter].setHorizontalAlignment(SwingConstants.RIGHT);
						
			nameLabel[counter] = new JLabel("--------------------");
			nameLabel[counter].setFont(new Font("Dialog", hoogsteScores.getFont().getStyle() | Font.BOLD, 15));
			
			/*
			 * Wisselen tussen rood en blauw in scoreboard
			 */
			if (toggle) {			
				scoreLabel[counter].setForeground(Color.BLUE);
				nameLabel[counter].setForeground(Color.BLUE);
				toggle = false;
			} else {
				scoreLabel[counter].setForeground(Color.RED);
				nameLabel[counter].setForeground(Color.RED);
				toggle = true;
			}
					
			zijBalkScores.add(scoreLabel[counter]);
			zijBalkNaam.add(nameLabel[counter]);
		}
		
		playerName = new JTextField("Anoniem");
		playerName.setBounds(10, 716, 183, 34);
		playerName.setFont(new Font("Dialog", playerName.getFont().getStyle() | Font.BOLD, 15));
		getContentPane().add(playerName);
				
		score = new JLabel("Score : ....");
		score.setBounds(203, 716, 109, 34);
		score.setFont(new Font("Dialog", score.getFont().getStyle() | Font.BOLD, 15));
		getContentPane().add(score);
		
		changeThema = new JButton("Thema");
		changeThema.setFont(changeThema.getFont().deriveFont(changeThema.getFont().getStyle() | Font.BOLD, 15f));
		changeThema.setBounds(320, 716, 109, 34);
		getContentPane().add(changeThema);
		
		gekozenThema = new JLabel("GekozenThema");
		gekozenThema.setFont(gekozenThema.getFont().deriveFont(gekozenThema.getFont().getStyle() | Font.BOLD, 15f));
		gekozenThema.setBounds(439, 716, 181, 34);
		getContentPane().add(gekozenThema);
	}

	public JPanel getGameBoard() {
		return gameBoard;
	}
	
	public JTextField getNaamJTextField() {
		return playerName;
	}
	
	public JLabel getScoreJLabel() {
		return score;
	}
	
	public JLabel[] getScoresJLabelArray() {
		return scoreLabel;
	}
	
	public JLabel[] getNamesJLabelArray() {
		return nameLabel;
		
	}
	
	public JButton getChangeThemaJButton() {
		return changeThema;
	}
	
	public JLabel getThemaJLabel() {
		return gekozenThema;
	}
}
