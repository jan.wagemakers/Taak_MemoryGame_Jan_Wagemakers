/*
 * Instantie van deze klasse wordt uitgevoerd als er op de knopthema wordt gedrukt.
 */
package be.janwagemakers.mgame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionClickOnThema implements ActionListener {
	private final String[] styles = {"emoji","tux","kat","vlag"};  		// de verschillende beschikbare thema's. 
	GUI gui;
	int counter;
	
	public ActionClickOnThema(GUI gui) {
		this.counter = 0;
		this.gui = gui;
	}
	
	/*
	 * Geef het thema als een string terug
	 */
	public String getThema() {
		updateThemaJLabel();											// in GUI de label thema tekst aanpassen
		return styles[counter];
	}

	/*
	 * Wordt uitgevoerd als op de knop thema wordt geklikt.
	 */
	@Override
	public void actionPerformed(ActionEvent click) {
		counter++;														// bij elke klik, doe counter++
		if ( counter >= styles.length ) counter = 0;					// bij overflow, wordt counter terug nul
		
		for (int c=0; c<Game.HOEVEEL; c++ ) {							// loop door alle kaarten op het scherm
			Kaart kaart = (Kaart) gui.getGameBoard().getComponent(c);		// haal de kaart op
			kaart.changeStyle(styles[counter]);								// verander de stijl van de kaart
		}
		updateThemaJLabel();											// in GUI de label thema tekst aanpassen
	}
	
	/*
	 * Aanpassen van label thema tekst.
	 */
	private void updateThemaJLabel() {
		gui.getThemaJLabel().setText(styles[counter]);
	}
}
