/*
 * Bijhouden van de hoogste scores.
 * 
 * Het opslagen van de hoogste scores in een database, leek mij geen goed idee omdat 
 * er dan steeds een SQL-server moet draaien om het spel te spelen. Daarom worden de hoogste 
 * scores opgeslagen in een gewoon tekst bestand "score.txt".
 * 
 * Voor het lezen & schrijven naar een bestand heb ik mij gebaseerd op:
 * 
 *   <https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html>
 *   
 */
package be.janwagemakers.mgame;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Scores {
	private static final String FILE = "score.txt";
	
	private ArrayList<String> scores;

	public Scores() {
		scores = new ArrayList<String>();
	}

	public void readScores() {
		scores.clear();															// Maak scores leeg, voor het geval er nog oude gegevens inzitten.
		
		String line = null;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(FILE);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            int counter = 0;
            String oldline = "";
            while((line = bufferedReader.readLine()) != null) {
            	if (!oldline.equals(line)) {									// Dubbele scores verwijderen.
            		// System.out.println(line);
            		scores.add(left(line));										// Scores toevoegen aan onze ArrayList (left() om lengte beperken).
            		counter++;
            		oldline = line;												
            	}
            	if (counter >= 10) break;										// We hebben enkel interesse in de 10 hoogste scores
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Bestand met hoogste scores niet gevonden.\nLijst met hoogste scores zal leeg zijn.");
        }
        catch(IOException ex) {
            System.out.println("Fout tijdens het lezen van hoogste score bestand.");
        }
 	}

	public void writeScores(int score, String naam) {
		scores.add(String.format("%03d", score) + " " + naam);					// %03d = aan score nullen toevoegen zodat sorteren goed werkt.
		Collections.sort(scores);
		Collections.reverse(scores);
		try {
			// Assume default encoding.
			FileWriter fileWriter = new FileWriter(FILE);

			// Always wrap FileWriter in BufferedWriter.
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Schrijf de scores naar het bestand
			for (String i : scores) {
				// System.out.println(i);

				// Note that write() does not automatically
				// append a newline character.
				bufferedWriter.write(left(i));
				bufferedWriter.newLine();
			}        
			// Always close files.
			bufferedWriter.close();
		}
		catch(IOException ex) {
			System.out.println("Fout tijdens het schrijven van hoogste score bestand.");
		}
	}
	
	/*
	 * Deze methode beperkt de lengte van de string tot max 20
	 */
	private String left(String string) {
		return string.substring(0, Math.min(20,string.length()));
	}
	
	/*
	 * Zet de lijst met scores om naar een StringArray, zodat de scores later in de GUI getoond kunnen worden.
	 */
	public String[] getScores() {
		int index = 0;
		String[] stringArray = new String[10];
		for (String i : scores) {
			char[] charArray = i.toCharArray();							// Zet String om naar charArray zo dat we aan de individuele chars kunnen.
			
			if (charArray[0] == '0') {									// Als String i begint met 00, dan vervangen we die door spaties
				charArray[0] = ' ';
				if (charArray[1] == '0') {							
					charArray[1] = ' ';
				}
			}
			i = String.valueOf(charArray);
						
			stringArray[index] = i;
			index++;
		}
		return stringArray;
	}
}
